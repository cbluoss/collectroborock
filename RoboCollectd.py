import os
import json
from time import sleep
import requests
from influxdb import InfluxDBClient

HOSTNAME = os.environ.get("HOSTNAME", "DancingQueen")
LOCATION = os.environ.get("LOCACTION", "TP")

INTERVAL = int(os.environ.get("INTERVAL", 60))
DEFAULT_TAGS = {'hostname': HOSTNAME, }


def read(baseUrl="http://localhost/"):
    # Valetudo API Endpoints:
    #   /api/current_status
    #   /api/consumable_status
    #   /api/timers


    def readStatus(baseUrl):
        response = requests.get(baseUrl + "/api/current_status")
        if response:
            status = response.json()
            status["clean_area"] = status["clean_area"] // 1000000 # in seconds
            json_data = {}
            json_data['measurement'] = "roborock_status"
            json_data['fields'] = {}
            for metric in ["state", "battery", "clean_time", "clean_area", "error_code", "map_present", "in_cleaning", "in_fresh_state", "lab_status", "fan_power", "dnd_enabled"]:
                json_data['fields'][metric] = status[metric]
            json_data['tags'] = DEFAULT_TAGS
            return json_data
        return False

    def readConsumableStatus(baseUrl):
        response = requests.get(baseUrl + "/api/consumable_status")
        if response:
            consumableStatus = response.json()["consumables"]
            # TODO: Add Summary Data (and understand it)
            json_data = {}
            json_data['measurement'] = "roborock_status"
            json_data['fields'] = {}
            for metric in ["main_brush_work_time", "side_brush_work_time", "filter_work_time", "sensor_dirty_time"]:
                json_data['fields'][metric] = consumableStatus[metric]
            json_data['tags'] = DEFAULT_TAGS
            return json_data
        return False

    def readTimers(baseUrl):
        response = requests.get(baseUrl + "/api/timers")
        if response:
            return response.json()
        return False

    status = readStatus(baseUrl)
    consumableStatus = readConsumableStatus(baseUrl)
    timers = readTimers(baseUrl)

    return [status, consumableStatus] #, timers


client = InfluxDBClient("192.168.41.26", 8086, "user", "pw", "hzh")
data = read("http://192.168.42.66")

client.write_points(data)
